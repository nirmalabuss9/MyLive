//
//  LoginVC.swift
//  MyLive
//
//  Created by Mithilesh kumar satnami on 22/09/21.
//

import UIKit

@available(iOS 13.0, *)
class LoginVC: BaseViewController {

    var yRef:CGFloat = 150.0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        yRef = (appDelegate.screenHeight/2)-300
        self.view.backgroundColor = GlobleVariable.shared.colorPrimaryTheme
        
        self.screenDesigning()
    }
    func screenDesigning(){
        
        let imgLogo:UIImageView = UIImageView()
        imgLogo.frame = CGRect(x: (appDelegate.screenWidth-200)/2, y: yRef, width: 200, height: 88)
        imgLogo.backgroundColor = UIColor.clear
        imgLogo.image = UIImage.init(named: "LogoImg")
        self.view.addSubview(imgLogo)
        
        yRef = yRef+imgLogo.frame.size.height+30
        
        let lblLogin:UILabel = UILabel()
        lblLogin.frame = CGRect(x: (appDelegate.screenWidth-200)/2, y: yRef, width: 200, height: 50)
        lblLogin.backgroundColor = UIColor.clear
        lblLogin.text = "Login"
        lblLogin.textColor = GlobleVariable.shared.colorText
        lblLogin.font = UIFont.init(name: GlobleVariable.shared.strFont, size: 24)
        lblLogin.textAlignment = .center
        lblLogin.clipsToBounds = true
        self.view.addSubview(lblLogin)
        
        yRef = yRef+lblLogin.frame.size.height+30
        
        let txtEmail = UITextField()
        txtEmail.frame = CGRect(x: 20, y: yRef, width: appDelegate.screenWidth-40, height: 35)
        txtEmail.backgroundColor = UIColor.clear
        txtEmail.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        txtEmail.font = GlobleVariable.shared.fontRegular
        txtEmail.textAlignment = .left
        txtEmail.setLeftPaddingPoints(0)
        txtEmail.setRightPaddingPoints(0)
        txtEmail.keyboardType = .emailAddress
        txtEmail.clipsToBounds = true
        txtEmail.textColor = UIColor.white
        self.view.addSubview(txtEmail)
        
        yRef = yRef+txtEmail.frame.size.height+2


        let lblLine1:UILabel = UILabel()
        lblLine1.frame = CGRect(x: 20, y: yRef, width: appDelegate.screenWidth-40, height: 1)
        lblLine1.backgroundColor = UIColor.white
        lblLine1.clipsToBounds = true
        self.view.addSubview(lblLine1)


        yRef = yRef+lblLine1.frame.size.height+20

        let txtPassword = UITextField()
        txtPassword.frame = CGRect(x: 20, y: yRef, width: appDelegate.screenWidth-40, height: 35)
        txtPassword.backgroundColor = UIColor.clear
        txtPassword.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        txtPassword.font = GlobleVariable.shared.fontRegular
        txtPassword.textAlignment = .left
        txtPassword.setLeftPaddingPoints(0)
        txtPassword.setRightPaddingPoints(0)
        txtPassword.keyboardType = .emailAddress
        txtPassword.clipsToBounds = true
        txtPassword.textColor = UIColor.white
        self.view.addSubview(txtPassword)
        
        yRef = yRef+txtPassword.frame.size.height+2

        let lblLine2:UILabel = UILabel()
        lblLine2.frame = CGRect(x: 20, y: yRef, width: appDelegate.screenWidth-40, height: 1)
        lblLine2.backgroundColor = UIColor.white
        lblLine2.clipsToBounds = true
        self.view.addSubview(lblLine2)


        yRef = yRef+lblLine2.frame.size.height+40

        let btnLogin:UIButton = UIButton()
        btnLogin.frame = CGRect(x: 20, y: yRef, width: appDelegate.screenWidth-40, height: 44)
        btnLogin.backgroundColor = GlobleVariable.shared.colorText
        btnLogin.setTitle("Login", for: .normal)
        btnLogin.layer.cornerRadius = 22
        btnLogin.titleLabel?.font = GlobleVariable.shared.fontRegular
        btnLogin.setTitleColor(UIColor.white, for: .normal)
        btnLogin.clipsToBounds = true
        btnLogin.addTarget(self, action: #selector(self.btnLoginClicked(_:)), for: .touchUpInside)
        self.view.addSubview(btnLogin)

        yRef = yRef+btnLogin.frame.size.height+40

        let lblLoginwith:UILabel = UILabel()
        lblLoginwith.frame = CGRect(x: (appDelegate.screenWidth-180)/2, y: yRef, width: 180, height: 30)
        lblLoginwith.backgroundColor = UIColor.clear
        lblLoginwith.text = "LOG IN WITH"
        lblLoginwith.textColor = UIColor.white
        lblLoginwith.font = GlobleVariable.shared.fontBoldSmall
        lblLoginwith.textAlignment = .center
        lblLoginwith.clipsToBounds = true
        self.view.addSubview(lblLoginwith)

        yRef = yRef+lblLoginwith.frame.size.height+20
        
        let btnGoogleLogin:UIButton = UIButton()
        btnGoogleLogin.frame = CGRect(x: (appDelegate.screenWidth/2)-110, y: yRef, width: 20, height: 20)
        btnGoogleLogin.backgroundColor = UIColor.clear
        btnGoogleLogin.setBackgroundImage(UIImage.init(named: "Google"), for: .normal)
        btnGoogleLogin.clipsToBounds = true
        btnGoogleLogin.addTarget(self, action: #selector(self.btnGoogleClicked(_:)), for: .touchUpInside)
        self.view.addSubview(btnGoogleLogin)

        let btnLinkdinLogin:UIButton = UIButton()
        btnLinkdinLogin.frame = CGRect(x: (appDelegate.screenWidth/2)-60, y: yRef, width: 21, height: 21)
        btnLinkdinLogin.backgroundColor = UIColor.white
        btnLinkdinLogin.setBackgroundImage(UIImage.init(named: "Vector"), for: .normal)
        btnLinkdinLogin.layer.borderWidth = 0.5
        btnLinkdinLogin.layer.borderColor = UIColor.black.cgColor
        btnLinkdinLogin.clipsToBounds = true
        btnLinkdinLogin.addTarget(self, action: #selector(self.btnGoogleClicked(_:)), for: .touchUpInside)
        self.view.addSubview(btnLinkdinLogin)

        let btnTwiterLogin:UIButton = UIButton()
        btnTwiterLogin.frame = CGRect(x: (appDelegate.screenWidth/2)-10, y: yRef, width: 20, height: 20)
        btnTwiterLogin.backgroundColor = UIColor.clear
        btnTwiterLogin.setBackgroundImage(UIImage.init(named: "Twiter"), for: .normal)
        btnTwiterLogin.clipsToBounds = true
        btnTwiterLogin.addTarget(self, action: #selector(self.btnTwiterClicked(_:)), for: .touchUpInside)
        self.view.addSubview(btnTwiterLogin)

        let btnFacebookLogin:UIButton = UIButton()
        btnFacebookLogin.frame = CGRect(x: (appDelegate.screenWidth/2)+40, y: yRef, width: 20, height: 20)
        btnFacebookLogin.backgroundColor = UIColor.clear
        btnFacebookLogin.setBackgroundImage(UIImage.init(named: "Facebook"), for: .normal)
        btnFacebookLogin.clipsToBounds = true
        btnFacebookLogin.addTarget(self, action: #selector(self.btnFacebookClicked(_:)), for: .touchUpInside)
        self.view.addSubview(btnFacebookLogin)

        let btnAppleLogin:UIButton = UIButton()
        btnAppleLogin.frame = CGRect(x: (appDelegate.screenWidth/2)+90, y: yRef-1, width: 19, height: 22)
        btnAppleLogin.backgroundColor = UIColor.clear
        btnAppleLogin.setBackgroundImage(UIImage.init(named: "Apple"), for: .normal)
        btnAppleLogin.clipsToBounds = true
        btnAppleLogin.addTarget(self, action: #selector(self.btnAppleLoginClicked(_:)), for: .touchUpInside)
        self.view.addSubview(btnAppleLogin)

    }
    
    //MARK: Action
    
    @objc func btnGoogleClicked(_ sender: UIButton){
        
    }
    @objc func btnLoginClicked(_ sender: UIButton){
        
    }
    @objc func btnTwiterClicked(_ sender: UIButton){
        
    }
    @objc func btnFacebookClicked(_ sender: UIButton){
        
    }
    @objc func btnAppleLoginClicked(_ sender: UIButton){
        
    }

}
