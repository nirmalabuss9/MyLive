//
//  BaseViewController.swift
//  Monadie
//
//  Created by Dilip Technology on 26/02/18.
//  Copyright © 2018 Dilip Technology. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class BaseViewController: UIViewController {

    var bannerHeight:CGFloat = 50
    var headerHeight:CGFloat = 72
    var menuHeight:CGFloat = 50
    var fontDiff:CGFloat = -2.0
    let voxFont:String = "VoxRound-Semibold"
    
    let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
    var isFromViewDidLoad: Bool = true
    
    let strAppName:String = "Colosseum"
    
    var keyboardHeight:CGFloat = 333.0

    let imgViewBG:UIImageView = UIImageView()

    override func viewDidLoad() {
        super.viewDidLoad()

        imgViewBG.frame = CGRect(x:0, y:0, width:appDelegate.screenWidth, height:appDelegate.screenHeight)
        imgViewBG.image = UIImage.init(named: "")
        self.view.addSubview(imgViewBG)

//        if(self.appDelegate.isiPad == true){
//            bannerHeight = 90
//        }
        self.view.backgroundColor = UIColor.clear
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
    }

//    func LoaderStart() {
//        IndicatorView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
//        IndicatorView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
//        self.view.addSubview(IndicatorView)
//        
//        let viewloader:UIView = UIView()
//        viewloader.frame = CGRect(x:(UIScreen.main.bounds.size.width-50)/2.0, y:(UIScreen.main.bounds.size.height-50)/2.0, width:50, height:50)
//        viewloader.backgroundColor = UIColor.lightGray
//        viewloader.layer.cornerRadius = 5.0
//        IndicatorView.addSubview(viewloader)
//        
//        activityindicator.frame = CGRect(x: 0, y: 0, width: 46, height: 46)
//        activityindicator.hidesWhenStopped = false
//        activityindicator.style = UIActivityIndicatorView.Style.gray
//        activityindicator.color = UIColor.red
//        activityindicator.startAnimating()
//        viewloader.addSubview(activityindicator)
//    }
//
//    func stopLodre()
//    {
//        activityindicator.stopAnimating()
//        IndicatorView.removeFromSuperview()
//    }
    
    static func showAlert(viewCtrl: UIViewController, title: String, message: String, completion: (()->Swift.Void)?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            (_: UIAlertAction) -> Void in
            if completion != nil {
                completion!()
            }
        }
        ;        alertController.addAction(okAction)
        DispatchQueue.main.async {
            viewCtrl.present(alertController, animated: true, completion: nil)
        }
    }

    func ShowLoader(viewCon: UIViewController)
    {
        let IndicatorView = UIView()
        IndicatorView.frame = CGRect(x: 0, y: 0, width: appDelegate.screenWidth, height: appDelegate.screenHeight)
        IndicatorView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        viewCon.view.addSubview(IndicatorView)

        let viewloader = UIView()
        viewloader.frame = CGRect(x:(appDelegate.screenWidth-50)/2.0, y:(appDelegate.screenHeight-50)/2.0, width:50, height:50)
        viewloader.backgroundColor = UIColor.lightGray
        viewloader.layer.cornerRadius = 5.0
        IndicatorView.addSubview(viewloader)

        let activityindicator = UIActivityIndicatorView()
        activityindicator.frame = CGRect(x: 0, y: 0, width: 46, height: 46)
        activityindicator.hidesWhenStopped = false
        activityindicator.style = UIActivityIndicatorView.Style.gray
        activityindicator.color = UIColor.red
        activityindicator.startAnimating()
        viewloader.addSubview(activityindicator)

    }
    func checkLowercase(_ text : String) -> Bool{
        let capitalLetterRegEx  = ".*[a-z]+.*"
        let texttest = NSPredicate(format:"SELF MATCHES %@", capitalLetterRegEx)
        let capitalresult = texttest.evaluate(with: text)
        print("\(capitalresult)")
        return capitalresult
    }
    
    func checkUpercase(_ text : String) -> Bool{
        let capitalLetterRegEx  = ".*[A-Z]+.*"
        let texttest = NSPredicate(format:"SELF MATCHES %@", capitalLetterRegEx)
        let capitalresult = texttest.evaluate(with: text)
        print("\(capitalresult)")
        return capitalresult
    }
    func checkNumber(_ text : String) -> Bool{
        let numberRegEx  = ".*[0-9]+.*"
        let texttest1 = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        let numberresult = texttest1.evaluate(with: text)
        print("\(numberresult)")
        
        return numberresult
    }
    func checkSpecialCharecter(_ text : String) -> Bool{
        let specialCharacterRegEx  = ".*[!&^%$#@()/]+.*"
        let texttest2 = NSPredicate(format:"SELF MATCHES %@", specialCharacterRegEx)
        let specialresult = texttest2.evaluate(with: text)
        print("\(specialresult)")
        return specialresult
    }

    // MARK: - Comomn Methods
    func showAlertMessage(_ strMessage: String) {
        let alert = UIAlertController(title: "", message: strMessage, preferredStyle: .alert)
        let yesButton = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            //Handle your yes please button action here
        })
        alert.addAction(yesButton)
        self.present(alert, animated: true, completion: nil)
    }
    
    func showUpgradeMessage(_ strMessage: String) {
        let alert = UIAlertController(title: "", message: "Please upgrade to premium", preferredStyle: .alert)
        let yesButton = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            self.gotoSubscribe()
        })
        alert.addAction(yesButton)
        let noButton = UIAlertAction(title: "Cancel", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            
//            if(strMessage == ""){
//                self.navigationController?.popToViewController(self.appDelegate.objHomeVC, animated: true)
//            }
        })
        alert.addAction(noButton)
        self.present(alert, animated: true, completion: nil)
    }

    func gotoSubscribe()
    {
//        let objSubscribe:SubscribeViewController = SubscribeViewController()
//        objSubscribe.view.backgroundColor = UIColor.init(white: 0.9, alpha: 1.0)
//        objSubscribe.view.frame = CGRect(x:0, y:72, width:appDelegate.screenWidth, height: appDelegate.screenHeight-72)
//        if(self.navigationController != nil) {
//            self.navigationController?.pushViewController(objSubscribe, animated: false)
//        }
//        else {
//            self.appDelegate.navController?.pushViewController(objSubscribe, animated: false)
//        }
    }

    func showAlertMessage(_ strMessage: String, withTitle strTitle: String) {
        
        let alert = UIAlertController(title: strTitle, message: strMessage, preferredStyle: .alert)
        let yesButton = UIAlertAction(title: "OK", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            //Handle your yes please button action here
        })
        alert.addAction(yesButton)
        self.present(alert, animated: true, completion: nil)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:-  UITextField Methods
    func animateTextField(_ textField:UITextField, up:Bool)
    {
        print("Animate ")
        let movementDistance:CGFloat = -130
        let movementDuration: Double = 0.3
        
        var movement:CGFloat = 0
        if up
        {
            movement = movementDistance
        }
        else
        {
            movement = -movementDistance
        }
        UIView.beginAnimations("animateTextField", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        //CGRectOffset(self.view.frame, 0, movement)
        UIView.commitAnimations()
    }

    func isValidEmail(_ testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
  func isValidPassword(password: String) -> Bool {
        let passwordRegEx = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*()\\-_=+{}|?>.<,:;~`’]{8,}$"
        let passwordTest = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
        let result = passwordTest.evaluate(with: password)
        return result
    }

    @objc func keyboardWillShow(_ notification: Notification) {

        //let yOffset:CGFloat = self.view.frame.origin.y
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            self.keyboardHeight = keyboardRectangle.height
            print("keyboardHeight = ", self.keyboardHeight)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
