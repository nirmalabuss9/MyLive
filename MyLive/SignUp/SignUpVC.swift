//
//  SignUpVC.swift
//  MyLive
//
//  Created by Mithilesh kumar satnami on 22/09/21.
//

import UIKit

@available(iOS 13.0, *)
class SignUpVC: BaseViewController {
    
    var yRef:CGFloat = 150.0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        yRef = (appDelegate.screenHeight/2)-300
        self.view.backgroundColor = GlobleVariable.shared.colorPrimaryTheme
        
        self.screenDesigning()
    }
    func screenDesigning(){
        
        let imgLogo:UIImageView = UIImageView()
        imgLogo.frame = CGRect(x: (appDelegate.screenWidth-200)/2, y: yRef, width: 200, height: 88)
        imgLogo.backgroundColor = UIColor.clear
        imgLogo.image = UIImage.init(named: "LogoImg")
        self.view.addSubview(imgLogo)
        
        yRef = yRef+imgLogo.frame.size.height+30
        
        let lblSignUp:UILabel = UILabel()
        lblSignUp.frame = CGRect(x: (appDelegate.screenWidth-200)/2, y: yRef, width: 200, height: 50)
        lblSignUp.backgroundColor = UIColor.clear
        lblSignUp.text = "Sign Up"
        lblSignUp.textColor = GlobleVariable.shared.colorText
        lblSignUp.font = UIFont.init(name: GlobleVariable.shared.strFont, size: 24)
        lblSignUp.textAlignment = .center
        lblSignUp.clipsToBounds = true
        self.view.addSubview(lblSignUp)
        
        yRef = yRef+lblSignUp.frame.size.height+30
        
        let txtNameSurname = UITextField()
        txtNameSurname.frame = CGRect(x: 20, y: yRef, width: appDelegate.screenWidth-40, height: 35)
        txtNameSurname.backgroundColor = UIColor.clear
        txtNameSurname.attributedPlaceholder = NSAttributedString(string: "Name and Surname", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        txtNameSurname.font = GlobleVariable.shared.fontRegular
        txtNameSurname.textAlignment = .left
        txtNameSurname.setLeftPaddingPoints(0)
        txtNameSurname.setRightPaddingPoints(0)
        txtNameSurname.keyboardType = .emailAddress
        txtNameSurname.clipsToBounds = true
        txtNameSurname.textColor = UIColor.white
        self.view.addSubview(txtNameSurname)
        
        yRef = yRef+txtNameSurname.frame.size.height+2

        let lblLine:UILabel = UILabel()
        lblLine.frame = CGRect(x: 20, y: yRef, width: appDelegate.screenWidth-40, height: 1)
        lblLine.backgroundColor = UIColor.white
        lblLine.clipsToBounds = true
        self.view.addSubview(lblLine)

        yRef = yRef+lblLine.frame.size.height+20

        let txtEmail = UITextField()
        txtEmail.frame = CGRect(x: 20, y: yRef, width: appDelegate.screenWidth-40, height: 35)
        txtEmail.backgroundColor = UIColor.clear
        txtEmail.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        txtEmail.font = GlobleVariable.shared.fontRegular
        txtEmail.textAlignment = .left
        txtEmail.setLeftPaddingPoints(0)
        txtEmail.setRightPaddingPoints(0)
        txtEmail.keyboardType = .emailAddress
        txtEmail.clipsToBounds = true
        txtEmail.textColor = UIColor.white
        self.view.addSubview(txtEmail)
        
        yRef = yRef+txtEmail.frame.size.height+2

        let lblLine1:UILabel = UILabel()
        lblLine1.frame = CGRect(x: 20, y: yRef, width: appDelegate.screenWidth-40, height: 1)
        lblLine1.backgroundColor = UIColor.white
        lblLine1.clipsToBounds = true
        self.view.addSubview(lblLine1)

        yRef = yRef+lblLine1.frame.size.height+20

        let txtPassword = UITextField()
        txtPassword.frame = CGRect(x: 20, y: yRef, width: appDelegate.screenWidth-40, height: 35)
        txtPassword.backgroundColor = UIColor.clear
        txtPassword.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        txtPassword.font = GlobleVariable.shared.fontRegular
        txtPassword.textAlignment = .left
        txtPassword.setLeftPaddingPoints(0)
        txtPassword.setRightPaddingPoints(0)
        txtPassword.keyboardType = .emailAddress
        txtPassword.clipsToBounds = true
        txtPassword.textColor = UIColor.white
        self.view.addSubview(txtPassword)
        
        yRef = yRef+txtPassword.frame.size.height+2

        let lblLine2:UILabel = UILabel()
        lblLine2.frame = CGRect(x: 20, y: yRef, width: appDelegate.screenWidth-40, height: 1)
        lblLine2.backgroundColor = UIColor.white
        lblLine2.clipsToBounds = true
        self.view.addSubview(lblLine2)

        yRef = yRef+lblLine2.frame.size.height+20

        let txtDOB = UITextField()
        txtDOB.frame = CGRect(x: 20, y: yRef, width: (appDelegate.screenWidth/2)-30, height: 35)
        txtDOB.backgroundColor = UIColor.clear
        txtDOB.attributedPlaceholder = NSAttributedString(string: "Date of birth", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        txtDOB.font = GlobleVariable.shared.fontRegular
        txtDOB.textAlignment = .left
        txtDOB.setLeftPaddingPoints(0)
        txtDOB.setRightPaddingPoints(15)
        txtDOB.keyboardType = .emailAddress
        txtDOB.clipsToBounds = true
        txtDOB.textColor = UIColor.white
        self.view.addSubview(txtDOB)
        
        let imgArrow1:UIImageView = UIImageView()
        imgArrow1.frame = CGRect(x: txtDOB.frame.size.width-13, y: 7.5, width: 13, height: 20)
        imgArrow1.backgroundColor = UIColor.clear
        imgArrow1.image = UIImage.init(named: "ArrowUD")
        txtDOB.addSubview(imgArrow1)

        let txtCountry = UITextField()
        txtCountry.frame = CGRect(x: (appDelegate.screenWidth/2)+10, y: yRef, width: (appDelegate.screenWidth/2)-30, height: 35)
        txtCountry.backgroundColor = UIColor.clear
        txtCountry.attributedPlaceholder = NSAttributedString(string: "Country", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        txtCountry.font = GlobleVariable.shared.fontRegular
        txtCountry.textAlignment = .left
        txtCountry.setLeftPaddingPoints(0)
        txtCountry.setRightPaddingPoints(15)
        txtCountry.keyboardType = .emailAddress
        txtCountry.clipsToBounds = true
        txtCountry.textColor = UIColor.white
        self.view.addSubview(txtCountry)
        
        let imgArrow2:UIImageView = UIImageView()
        imgArrow2.frame = CGRect(x: txtCountry.frame.size.width-13, y: 7.5, width: 13, height: 20)
        imgArrow2.backgroundColor = UIColor.clear
        imgArrow2.image = UIImage.init(named: "ArrowUD")
        txtCountry.addSubview(imgArrow2)

        yRef = yRef+txtDOB.frame.size.height+2

        let lblLine3:UILabel = UILabel()
        lblLine3.frame = CGRect(x: 20, y: yRef, width: (appDelegate.screenWidth/2)-30, height: 1)
        lblLine3.backgroundColor = UIColor.white
        lblLine3.clipsToBounds = true
        self.view.addSubview(lblLine3)

        let lblLine4:UILabel = UILabel()
        lblLine4.frame = CGRect(x: (appDelegate.screenWidth/2)+10, y: yRef, width: (appDelegate.screenWidth/2)-30, height: 1)
        lblLine4.backgroundColor = UIColor.white
        lblLine4.clipsToBounds = true
        self.view.addSubview(lblLine4)

        yRef = yRef+lblLine4.frame.size.height+20

        let txtCity = UITextField()
        txtCity.frame = CGRect(x: 20, y: yRef, width: (appDelegate.screenWidth/2)-30, height: 35)
        txtCity.backgroundColor = UIColor.clear
        txtCity.attributedPlaceholder = NSAttributedString(string: "City", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        txtCity.font = GlobleVariable.shared.fontRegular
        txtCity.textAlignment = .left
        txtCity.setLeftPaddingPoints(0)
        txtCity.setRightPaddingPoints(15)
        txtCity.keyboardType = .emailAddress
        txtCity.clipsToBounds = true
        txtCity.textColor = UIColor.white
        self.view.addSubview(txtCity)
        
        let imgArrow3:UIImageView = UIImageView()
        imgArrow3.frame = CGRect(x: txtCity.frame.size.width-13, y: 7.5, width: 13, height: 20)
        imgArrow3.backgroundColor = UIColor.clear
        imgArrow3.image = UIImage.init(named: "ArrowUD")
        txtCity.addSubview(imgArrow3)

        let txtZipCode = UITextField()
        txtZipCode.frame = CGRect(x: (appDelegate.screenWidth/2)+10, y: yRef, width: (appDelegate.screenWidth/2)-30, height: 35)
        txtZipCode.backgroundColor = UIColor.clear
        txtZipCode.attributedPlaceholder = NSAttributedString(string: "ZIP Code", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        txtZipCode.font = GlobleVariable.shared.fontRegular
        txtZipCode.textAlignment = .left
        txtZipCode.setLeftPaddingPoints(0)
        txtZipCode.setRightPaddingPoints(15)
        txtZipCode.keyboardType = .emailAddress
        txtZipCode.clipsToBounds = true
        txtZipCode.textColor = UIColor.white
        self.view.addSubview(txtZipCode)
        
        let imgArrow4:UIImageView = UIImageView()
        imgArrow4.frame = CGRect(x: txtZipCode.frame.size.width-13, y: 7.5, width: 13, height: 20)
        imgArrow4.backgroundColor = UIColor.clear
        imgArrow4.image = UIImage.init(named: "ArrowUD")
        txtZipCode.addSubview(imgArrow4)

        yRef = yRef+txtZipCode.frame.size.height+2

        let lblLine5:UILabel = UILabel()
        lblLine5.frame = CGRect(x: 20, y: yRef, width: (appDelegate.screenWidth/2)-30, height: 1)
        lblLine5.backgroundColor = UIColor.white
        lblLine5.clipsToBounds = true
        self.view.addSubview(lblLine5)

        let lblLine6:UILabel = UILabel()
        lblLine6.frame = CGRect(x: (appDelegate.screenWidth/2)+10, y: yRef, width: (appDelegate.screenWidth/2)-30, height: 1)
        lblLine6.backgroundColor = UIColor.white
        lblLine6.clipsToBounds = true
        self.view.addSubview(lblLine6)

        yRef = yRef+lblLine6.frame.size.height+40

        let btnRegister:UIButton = UIButton()
        btnRegister.frame = CGRect(x: 20, y: appDelegate.screenHeight-140, width: appDelegate.screenWidth-40, height: 44)
        btnRegister.backgroundColor = GlobleVariable.shared.colorText
        btnRegister.setTitle("Register", for: .normal)
        btnRegister.layer.cornerRadius = 22
        btnRegister.titleLabel?.font = GlobleVariable.shared.fontRegular
        btnRegister.setTitleColor(UIColor.white, for: .normal)
        btnRegister.clipsToBounds = true
        btnRegister.addTarget(self, action: #selector(self.btnRegisterClicked(_:)), for: .touchUpInside)
        self.view.addSubview(btnRegister)


        let lblAlreadyAcc:UILabel = UILabel()
        lblAlreadyAcc.frame = CGRect(x: (appDelegate.screenWidth-180)/2, y: appDelegate.screenHeight-68, width: 180, height: 20)
        lblAlreadyAcc.backgroundColor = UIColor.clear
        lblAlreadyAcc.text = "Already have an account?"
        lblAlreadyAcc.textColor = GlobleVariable.shared.colorGaryType
        lblAlreadyAcc.font = GlobleVariable.shared.fontRegularSmall
        lblAlreadyAcc.textAlignment = .center
        lblAlreadyAcc.clipsToBounds = true
        self.view.addSubview(lblAlreadyAcc)
        
        var attrs = [NSAttributedString.Key.font : GlobleVariable.shared.fontBoldSmall,NSAttributedString.Key.foregroundColor : UIColor.white,NSAttributedString.Key.underlineStyle : 1] as [NSAttributedString.Key : Any]
        var attributedString = NSMutableAttributedString(string:"")

        let btnLogin:UIButton = UIButton()
        btnLogin.frame = CGRect(x: (appDelegate.screenWidth-110)/2, y: appDelegate.screenHeight-45, width: 110, height: 20)
        btnLogin.backgroundColor = UIColor.clear
        let buttonTitleStr = NSMutableAttributedString(string:"Log in", attributes:attrs)
        attributedString.append(buttonTitleStr)
        btnLogin.setAttributedTitle(attributedString, for: .normal)
//        btnLogin.setTitle("Log in", for: .normal)
        btnLogin.titleLabel?.font = GlobleVariable.shared.fontBoldSmall
        btnLogin.setTitleColor(UIColor.white, for: .normal)
        btnLogin.clipsToBounds = true
        btnLogin.addTarget(self, action: #selector(self.btnLoginClicked(_:)), for: .touchUpInside)
        self.view.addSubview(btnLogin)

    }
    
    //MARK: Action
    
    @objc func btnLoginClicked(_ sender: UIButton){
        let vc:LoginVC = LoginVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func btnRegisterClicked(_ sender: UIButton){
        
    }

}
